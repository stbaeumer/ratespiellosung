class Game{

    constructor(){

        this.gesuchteZahl = parseInt((Math.random() * (100 - 0)) + 0);
        this.getippteZahl ;
        this.versuche = 0;
    }

    // Innerhalb von Klassen spricht man von Methoden. Methoden werden ohne Schlüsselwort definiert.

    zufallszahlGenerieren(){

        return parseInt(this.wert) + parseInt(this.wert);
    }
    
    zahlenVergleichen(){
        if (this.gesuchteZahl < this.getippteZahl) {
            return "Die gesuchte Zahl ist kleiner als die getippte Zahl";
        }    
        if (this.gesuchteZahl > this.getippteZahl) {
            return "Die gesuchte Zahl ist größer als die getippte Zahl";
        }    
        if (this.gesuchteZahl = this.getippteZahl) {
            return "Die gesuchte Zahl ist gleich mit der getippten Zahl. Sie haben " + this.versuche + " Versuche benötigt.";
        } 
    }
} // Ende der Klassendefintion

// 

let game = new Game();

// Außerhalb von Klassen spricht man von Funktionen. Funktionen werden mit Schlüsselwort definiert.

function btnZahlenVergleichenOnClick() {

    // Der Wert des Elements mit der Id 'tbxZahl' des HTML-Dokuments wird zugewiesen 
    // an die Eigenschaft wert des Objekts zahl.

    game.getippteZahl = document.getElementById('tbxZahl').value
    
    // In der Anweisung wird die Methode 'verdoppleZahl' auf das Objekts zahl aufgerufen und der 
    // Rückgabewert wird zugewiesen an die Eigenschaft an die Eigenschaft innerHTML des Elements
    // mit der Id 'lblAusgabe'. 

    document.getElementById("lblAusgabe").innerHTML = game.zahlenVergleichen();
    
    // Zusätzlich zur Ausgabe auf dem Label wird das Objekt auf der Console geloggt.
    // Das Loggen unterstützt das Debuggen
    game.versuche++; 
    console.log(game);

}


if(false)
{
    // Ab hier kommt die Datenbankfunktionalität

    // Einbinden der SQLite-Datenbank

    var sql = require('./sql.js');
    
    // Erstellen einer neuen Datenbank
    var db = new sql.Database();
    
    // SQL-Befehle werden als String an die Datenbank übergeben.

    db.run("CREATE TABLE test (col1 int, col2 char);");
    
    // Einfügen neuer Datensätze

    db.run("INSERT INTO test VALUES (11, 'Hello')");
    db.run("INSERT INTO test VALUES (12, 'World!')");
 
    // Prepare a statement
    var stmt = db.prepare("SELECT * FROM test");
    //stmt.getAsObject({$start:1, $end:1}); // {col1:1, col2:111}

    var result = stmt.getAsObject({':aval' : 1, ':bval' : 'world'});
    console.log(result); // Will print {a:1, b:'world'}

    // Bind new values
    stmt.bind({$start:1, $end:2});
    while(stmt.step()) { //
        var row = stmt.getAsObject();
        //var row = stmt.getAsObject({$start:1, $end:1});
        console.log(row);        
    }
    // Anlegen einer neuen Tabelle 
    //console.log(stmt);
    }

